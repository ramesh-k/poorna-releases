# Readme
This is Release repository of Poorna keyboards in Windows and Mac. Download the zip and install it. for linux it can be directly installed from the source code repo. installation instructions can found from Readme\
[Poorna Inscript linux](https://gitlab.com/smc/poorna/poorna-linux)\
[poorna Remington Linux](https://gitlab.com/smc/poorna/poorna-remington-linux)
## install in Windows
1. Download the release file
2. unzip
3. Double click setup to install

## install in Mac
1. Download the release file
2. unzip
3. press `Cmd+Shift+G`, and type “/Library/Keyboard Layouts/” to navigate to the Keyboard Layouts,
4. paste the poorna bundle here then add keyboard layout from keyboard settings.

-------
വിന്റോസ്, മാക് എന്നീ പ്ലാറ്റ്ഫോമുകളിലെ പൂൎണ്ണ കീബോർഡിന്റെ റിലീസ് റെപോസിറ്ററിയാണ് ഇത്. `zip` ഫയൽ ‍ഡൗൺലോഡ് ചെയ്ത് ഇൻസ്റ്റാൾ ചെയ്യുക. ലിനക്സിന് അതാത് സോഴ്സ് റെപോസിറ്ററിയിൽ നിന്നു തന്നെ നേരിട്ട് ഇൻസ്റ്റാൾ ചെയ്യാവുന്നതാണ്. ഇൻസ്റ്റളേഷൻ നിർദ്ദേശങ്ങൾ റീഡ്മി ഫയലിൽ ലഭ്യമാണ്\
[Poorna Inscript linux](https://gitlab.com/smc/poorna/poorna-linux)\
[poorna Remington Linux](https://gitlab.com/smc/poorna/poorna-remington-linux)

## വിന്റോസിലെ ഇൻസ്റ്റളേഷൻ
1. റിലീസ് zip ഫയൽ ഡൗൺലോഡ് ചെയ്യുക
2. അൺസിപ് ചെയ്യുക
3. setup എന്ന ഫയലിൽ ഡബിൾ ക്ലിക്ക് ചെയ്ത് ഇൻസ്റ്റളേഷൻ പൂർത്തിയാക്കുക.

## മാകിലെ ഇൻസ്റ്റളേഷൻ
1. റിലീസ് zip ഫയൽ ഡൗൺലോഡ് ചെയ്യുക
2. അൺസിപ് ചെയ്യുക
3. `Cmd+Shift+G` അമർത്തിയ ശേഷം “/Library/Keyboard Layouts/” എന്ന് ടൈപ് ചെയ്ത് എന്റർ പ്രസ് ചെയ്യുക. ശേഷം poorna bundle ഫയൽ ഈ ലോക്കേഷനിൽ പേസ്റ്റ് ചെയ്യുക. തുടർന്ന് കീബോർഡ് സെറ്റിംഗ്സിൽ നിന്നും poorna തെരഞ്ഞെടുക്കുക.